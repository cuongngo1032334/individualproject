IndividualProject::Application.routes.draw do
  get "home/index"
  get "report/completed"
  get "find_by_phone/index"
  resources :specialist_statuses

  resources :time_slots

  resources :statuses

  resources :appointments do
    collection do
      get 'patient/:patient_id', :action => 'new_appointment_from_patient'
    end
  end

  resources :specialists do
    collection do
      get 'manage_appointments'
    end
  end

  resources :insurances

  resources :diagnostic_codes

  resources :patients

  get '/availableSlots/:specialist_id/:date' => 'time_slots#availableSlots'

  get '/cancelAppointment/:appointment_id' => 'appointments#cancelAppointment'

  get '/find_by_phone/:phone' => 'find_by_phone#findByPhone'

  get '/amountFromCode/:codeID' => 'diagnostic_codes#getAmount'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
