require 'test_helper'

class SpecialistStatusesControllerTest < ActionController::TestCase
  setup do
    @specialist_status = specialist_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:specialist_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create specialist_status" do
    assert_difference('SpecialistStatus.count') do
      post :create, specialist_status: { name: @specialist_status.name }
    end

    assert_redirected_to specialist_status_path(assigns(:specialist_status))
  end

  test "should show specialist_status" do
    get :show, id: @specialist_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @specialist_status
    assert_response :success
  end

  test "should update specialist_status" do
    patch :update, id: @specialist_status, specialist_status: { name: @specialist_status.name }
    assert_redirected_to specialist_status_path(assigns(:specialist_status))
  end

  test "should destroy specialist_status" do
    assert_difference('SpecialistStatus.count', -1) do
      delete :destroy, id: @specialist_status
    end

    assert_redirected_to specialist_statuses_path
  end
end
