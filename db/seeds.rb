# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'open-uri'

#Populate diagnostic_codes table
# DiagnosticCode.delete_all
# open("/vagrant/IndividualProject/app/assets/diagcode.txt") do |codes|
#   codes.read.each_line do |code|
#     diagCode, desc, extra = code.chomp.split("|")
#     DiagnosticCode.create(:code => diagCode, :description => desc, :extra_amount => extra)
#   end
# end

# Populate insurances table
Insurance.delete_all
open ("/vagrant/IndividualProject/app/assets/insurance.txt") do |insurances|
  insurances.read.each_line do |insurance|
    name,address = insurance.chomp.split("|")
    Insurance.create(:name => name, :address => address)
  end
end

#Populate patients table
Patient.delete_all
open ("/vagrant/IndividualProject/app/assets/patient.txt") do |patients|
  patients.read.each_line do |patient|
    fname, lname, address, phone, insurance_id = patient.chomp.split("|")
    Patient.create(:first_name => fname, :last_name => lname, :address => address, :phone => phone, :insurance_id => insurance_id)
  end
end

#Populate specialists table
Specialist.delete_all
open("/vagrant/IndividualProject/app/assets/specialist.txt") do |specialists|
  specialists.read.each_line do |specialist|
    fname, lname, address, phone = specialist.chomp.split("|")
    Specialist.create(:first_name => fname, :last_name => lname, :address => address, :phone => phone)
  end
end
