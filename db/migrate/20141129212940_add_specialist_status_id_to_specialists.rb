class AddSpecialistStatusIdToSpecialists < ActiveRecord::Migration
  def change
    add_column :specialists, :specialist_status_id, :integer
  end
end
