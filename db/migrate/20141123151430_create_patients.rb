class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :phone
      t.integer :insurance_id

      t.timestamps
    end
  end
end
