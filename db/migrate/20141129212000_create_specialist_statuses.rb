class CreateSpecialistStatuses < ActiveRecord::Migration
  def change
    create_table :specialist_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
