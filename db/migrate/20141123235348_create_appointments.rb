class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :specialist_id
      t.integer :patient_id
      t.integer :status_id
      t.integer :diagnostic_code_id
      t.datetime :date
      t.text :reason
      t.float :amount

      t.timestamps
    end
  end
end
