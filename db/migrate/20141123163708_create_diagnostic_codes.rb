class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :code
      t.text :description
      t.float :extra_amount

      t.timestamps
    end
  end
end
