class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :specialists, through: :appointments
  belongs_to :insurance
  paginates_per 20
  validates :phone, uniqueness: true, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true

  def full_name
    first_name + ' ' + last_name
  end

  def self.findByPhone(phone)
    patient = Patient.find_by(:phone => phone)
    if patient.nil?
      false
    else
      patient.id
    end

  end
end
