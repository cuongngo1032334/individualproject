class DiagnosticCode < ActiveRecord::Base
  has_many :appointments
  validates :code, presence: true
  validates :description, presence: true
  validates :extra_amount, presence: true
  paginates_per 10
  def codeAndName
      code + ' - ' + description
  end
end
