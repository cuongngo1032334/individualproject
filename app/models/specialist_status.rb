class SpecialistStatus < ActiveRecord::Base
  validates :name, presence: true
  has_many :specialists
end
