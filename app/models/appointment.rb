class Appointment < ActiveRecord::Base
  belongs_to :specialist
  belongs_to :patient
  belongs_to :diagnostic_code
  belongs_to :status
  belongs_to :time_slot
  validates :date, presence: true
  validates :time_slot_id, presence: true
  validates :status_id, presence: true
  validates :patient_id, presence: true
  validates :specialist_id, presence: true
  validates :reason, presence: true
  paginates_per 10
  # def getAmount(diagCode)
  #   @appointment.DiagnosticCode.find(diagCode).extra_amount
  # end

  def self.cancelAppointment(appointment_id)
      appointment = Appointment.find(appointment_id)
      appDateTime = DateTime.parse(appointment.date.to_s + 'T' + appointment.time_slot.slot)
      now = DateTime.now
      if (appDateTime.to_i - now.to_i)/3600 >= 8
        appointment.update(status_id: 3)
        "true"
      else
        "false"
      end
  end

  def self.completedAppointment
    Appointment.where("status_id =?", 2)
  end


end
