class Specialist < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments
  belongs_to :specialist_status
  validates :specialist_status_id, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone, presence: true
  paginates_per 10
  def full_name
    first_name + ' ' + last_name
  end

  def self.allActive
    Specialist.where("specialist_status_id=?",1)
  end

  def numAppsLeftToday
    @specialist.appointments.where("status_id=? AND date=?",1,Date.today).count
  end
end
