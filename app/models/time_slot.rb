class TimeSlot < ActiveRecord::Base
  has_many :appointments
  validates :slot, presence: true

  def self.allAvailable(s_id, date)
    sql = "SELECT id FROM time_slots WHERE id NOT IN (SELECT id FROM time_slots WHERE id IN (SELECT time_slot_id FROM appointments WHERE specialist_id=" + s_id + " and date=\"" + date + "\" and status_id NOT IN(2,3)))"
    records_array =  ActiveRecord::Base.connection.execute(sql)
    idArray = Array.new
    records_array.each do |record|
      idArray.push(record["id"])
    end
    TimeSlot.find(idArray)
  end
end