json.array!(@insurances) do |insurance|
  json.extract! insurance, :id, :name, :address
  json.url insurance_url(insurance, format: :json)
end
