json.array!(@diagnostic_codes) do |diagnostic_code|
  json.extract! diagnostic_code, :id, :code, :description, :extra_amount
  json.url diagnostic_code_url(diagnostic_code, format: :json)
end
