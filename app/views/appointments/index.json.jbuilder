json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :specialist_id, :patient_id, :status_id, :diagnostic_code_id, :date, :reason, :amount
  json.url appointment_url(appointment, format: :json)
end
