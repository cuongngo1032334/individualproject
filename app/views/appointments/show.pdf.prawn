pdf.text "Dermatology Office", :align => :center, :size => 30, :style => :bold
pdf.text "----------", :align => :center, :size => 30
pdf.text "Patient name: #{@appointment.patient.full_name}", :size => 15

pdf.text "Phone: #{@appointment.patient.phone}", :size => 10
pdf.text "#{@appointment.patient.address}", :size => 10
pdf.text "*********************************************************************"
pdf.text "
Appointment Date: #{@appointment.date} at #{@appointment.time_slot.slot}",  :size => 15
pdf.text "Appointment number: #{@appointment.id}", :size => 10
pdf.text "Specialist name: #{@appointment.specialist.full_name}", :size => 10
pdf.text "*********************************************************************"
pdf.text "
Reason for coming: ", :size => 15
pdf.text "#{@appointment.reason}", :size => 10
pdf.text "*********************************************************************"
pdf.text "
Diagnostic: #{@appointment.diagnostic_code.codeAndName}", :size => 15
pdf.text "*********************************************************************"
pdf.text "
Specialist Notes: ", :size => 15
pdf.text "#{@appointment.note}", :size => 10
pdf.text "
#{number_to_currency(@appointment.amount)}", :size => 30, :style => :bold, :align => :right