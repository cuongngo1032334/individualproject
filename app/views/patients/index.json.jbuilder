json.array!(@patients) do |patient|
  json.extract! patient, :id, :first_name, :last_name, :address, :phone, :insurance_id
  json.url patient_url(patient, format: :json)
end
