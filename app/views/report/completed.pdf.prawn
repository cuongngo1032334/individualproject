pdf.text "Dermatology Office", :align => :center, :size => 30, :style => :bold
pdf.text "Completed Appointments Report", :align => :center, :size => 20, :style => :bold

apps = [["Specialist", "Patient", "Appoint Date", "Time", "Amount"]]
apps += @appointments.map do |app|
    [
        app.specialist.full_name,
        app.patient.full_name,
        app.date,
        app.time_slot.slot,
        number_to_currency(app.amount)
    ]
end

pdf.table(apps, :header => true)