json.array!(@specialist_statuses) do |specialist_status|
  json.extract! specialist_status, :id, :name
  json.url specialist_status_url(specialist_status, format: :json)
end
