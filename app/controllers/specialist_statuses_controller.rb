class SpecialistStatusesController < ApplicationController
  before_action :set_specialist_status, only: [:show, :edit, :update, :destroy]

  # GET /specialist_statuses
  # GET /specialist_statuses.json
  def index
    @specialist_statuses = SpecialistStatus.all
  end

  # GET /specialist_statuses/1
  # GET /specialist_statuses/1.json
  def show
  end

  # GET /specialist_statuses/new
  def new
    @specialist_status = SpecialistStatus.new
  end

  # GET /specialist_statuses/1/edit
  def edit
  end

  # POST /specialist_statuses
  # POST /specialist_statuses.json
  def create
    @specialist_status = SpecialistStatus.new(specialist_status_params)

    respond_to do |format|
      if @specialist_status.save
        format.html { redirect_to @specialist_status, notice: 'Specialist status was successfully created.' }
        format.json { render action: 'show', status: :created, location: @specialist_status }
      else
        format.html { render action: 'new' }
        format.json { render json: @specialist_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /specialist_statuses/1
  # PATCH/PUT /specialist_statuses/1.json
  def update
    respond_to do |format|
      if @specialist_status.update(specialist_status_params)
        format.html { redirect_to @specialist_status, notice: 'Specialist status was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @specialist_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specialist_statuses/1
  # DELETE /specialist_statuses/1.json
  def destroy
    @specialist_status.destroy
    respond_to do |format|
      format.html { redirect_to specialist_statuses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_specialist_status
      @specialist_status = SpecialistStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def specialist_status_params
      params.require(:specialist_status).permit(:name)
    end
end
