class FindByPhoneController < ApplicationController
  def index

  end

  def findByPhone
    response = Patient.findByPhone(params[:phone])
    render json: response
  end
end
