# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
  $("#submit").click ->
    $.getJSON "/find_by_phone" + "/" + $("#phoneNumber").val(), {}, (json, response) ->
      if json is false
        alert("Your phone number does not match any record. Please try again or click New Patient")
      else
        window.location.replace("/patients/"+json)
      return
    return
  return


$(document).ready(ready)
$(document).on('page:load', ready)