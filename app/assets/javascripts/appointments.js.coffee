# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
getMonth = (monVal) ->
  if monVal < 10
    "0" + monVal
  else
    monVal
  return

getAvailableSlots = ->
  $.getJSON "/availableSlots/" + $("#appointment_specialist_id").val() + "/" + $("#appointment_date").val() , {}, (json, response) ->
    $timeSlot = $("#appointment_time_slot_id")
    $timeSlot.empty() # remove old options
    json.forEach (obj) ->
      $timeSlot.append $("<option></option>").attr("value", obj.id).text(obj.slot)
      return
    return


goToPatientShowPage = ->
  window.location.replace("/patients/" + $("#patient_id").text())
  return

cancelAppointment = ->
  $.getJSON "/cancelAppointment" + "/" + $("#appointment_id").text(), {}, (json, response) ->
    if json is true
      alert "appointment is cancelled"
      goToPatientShowPage()
    else
      alert "appointment can only be cancelled 8 hours before appointment time"
    return
  return

fillAmount = ->
  $.getJSON "/amountFromCode" + "/" + $("#appointment_diagnostic_code_id option:selected").val(), {}, (json, response) ->
    $("#appointment_amount").val(json)
    $("#appointment_amount").text(json)
    return
  return

hideFieldsInPatientView = ->
  $(".patient-view").hide()
  return

hideAppID = ->
  $("#appointment_id").hide()
  return

hidePatientID = ->
  $("#patient_id").hide()
  return

ready = ->

  hidePatientID()
  hideAppID()
  hideFieldsInPatientView()

  $("#appointment_diagnostic_code_id").change ->
    fillAmount()
    return

  $("#cancelAppointment").click ->
    cancelAppointment()
    return

  $("#backButton").click ->
    goToPatientShowPage()
    return

  $("#appointment_date").datepicker(
    beforeShowDay: $.datepicker.noWeekends
    dateFormat: "yy-mm-dd"
    defaultDate: new Date()
  ).attr "readonly", "readonly"

  $("#appointment_date").datepicker "setDate", new Date()

  $("#appointment_date").change ->
    getAvailableSlots()
    return

  $("#appointment_reason").val("N/A")

  $("#appointment_reason").change ->
    $("#appointment_reason").val("N/A")  unless $("#appointment_reason").val()
    return

  $("#appointment_specialist_id").change ->
    getAvailableSlots()
    return


$(document).ready(ready)
$(document).on('page:load', ready)